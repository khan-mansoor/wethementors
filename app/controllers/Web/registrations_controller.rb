class Web::RegistrationsController < Devise::RegistrationsController

  # layout 'sessions'

  def new
    @web_user = User.new
    @web_user.email = "a1@wtm.com"
    @web_user.first_name = 'A1'
    @web_user.last_name = 'B1'
    @web_user.password = '123456'
    @web_user.password_confirmation = '123456'
  end

  def create
    permitted_params = sign_up_params
    @web_user.new(permitted_params)
    @error = nil
    binding.pry
    if @web_user.save
      redirect_to root_path(show_welcome_message: true)
      return
    else
      render action: 'new'
    end
  rescue => e
    puts "Error creating new user due to: #{e.message}"
    @error = 'Something went wrong!'
  end

  private

  def sign_up_params
    params.require(:user).permit(:first_name, :last_name, :email, :gender, :password, :password_confirmation)
  end

end
